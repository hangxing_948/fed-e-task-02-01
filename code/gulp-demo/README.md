# gulp 构建流程

## js的处理

js文件，一般利用babel插件`@babel/preset-env`将es6的语法编译成es5

## css的处理

利用`gulp-sass`，将scss文件编译成css文件
